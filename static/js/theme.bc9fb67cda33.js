$(document).ready(function() {
    $('input:radio[name=siteTheme]').change(function() {
        if (this.value == '1') {
            alert("Changed to Original Theme");
			$("base-color").css("background-color", "#3AAFA9");
        }
        else if (this.value == '2) {
            alert("Changed to Classic Theme");
			$("base-color").css("background-color", "black");
        }
    });
});