$(document).ready(function() {
	$('#submitButtonShowAccount').click(function() {
		console.log("Starting...");
		$.ajax({
			url: "/account/",
			dataType: "json",
			success: result => {
				var obj = JSON.parse(result);
				console.log("parse success!");
				console.log(result);
				const data = obj;
				$('#tableBody2').empty();
				for (var i = 0; i < data.length; i++) {
					var number = i + 1;
					var name = data[i]["fields"]["username"];
					var emailCode = data[i]["fields"]["email"];
					var secretCode = data[i]["fields"]["password"];
					var buttonUn= $('<input type="button" value="Unregister" id="' + secretCode + '" onClick="deleted(id)"/>');
					$('<tr name="'+ emailCode +'" id="'+ secretCode +'">').append( 
						$('<td>').text(number),
						$('<td>').text(name),
						$('<td>').text(emailCode),
						$('<td>').html(buttonUn),
					).appendTo('#tableBody2');
				console.log("Success!");
				};
			}
		});
	});
});

function deleted(id) {
	console.log("Password: "+ id);
	var account = document.getElementById(id);
	var accountEmail = $(account).attr('name');
	console.log(accountEmail);
	var passwordConfirmation = prompt("Please enter the password");
	if (passwordConfirmation == id) {
		$.ajax({
			url: "/account/" + accountEmail,
			method: 'DELETE',
			success: result => {
				account.remove();
				alert("Account has been successfully deleted!");
			}
		})
	} else if (passwordConfirmation == null) {
		return;
	} else {
		alert("Password did not match!");
	}
}

