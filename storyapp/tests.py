from django.test import TestCase
from .models import Status
from django.contrib.auth import get_user_model
from django.test import Client
from django.urls import resolve
from .views import index, status_new, profile
from .forms import PostForm
from .models import Status
from django.contrib.auth.models import User

from selenium import webdriver
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class StatusModelTest(TestCase):
	def test_string_representation(self):
		status = Status(title="Test")
		self.assertEqual(str(status), status.title)
	
	def test_verbose_name_plural(self):
		self.assertEqual(str(Status._meta.verbose_name_plural), "status")
		
	def test_description(self):
		testing = Status(description = "test")
		self.assertNotEqual(str(testing), testing.description)

class UnitTest(TestCase):
    
		def test_url_is_exist(self):
			response = Client().get('/')
			self.assertEqual(response.status_code, 200)
			
		def test_using_index_func(self):
			found = resolve('/')
			self.assertEqual(found.func, index)
			
		def test_no_entries(self):
			response = self.client.get('/')
			self.assertContains(response, 'No entries yet.')
			
		def test_model_can_create_new_todo(self):
            # Creating a new activity
			new_activity = Status.objects.create(title='mengerjakan lab ppw', description='mengerjakan lab_5 ppw')
    
            # Retrieving all available activity
			counting_all_available_todo = Status.objects.all().count()
			self.assertEqual(counting_all_available_todo, 1)

			
class HomePageTests(TestCase):

    """Test whether status show up on the homepage"""

    def test_one_Status(self):
        Status.objects.create(title='1-title', description='1-description')
        response = self.client.get('/')
        self.assertContains(response, '1-title')
        self.assertContains(response, '1-description')
		
class ProfileTests(TestCase):
	def test_call_profile(self):
		response = self.client.get('/profile/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'profile.html')
		
class ThemeTests(TestCase):
	def test_call_theme(self):
		response = self.client.get('/theme/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'change_theme.html')
		
class NewTests(TestCase):
	def test_call_new(self):
		response = self.client.get('/new/')
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'status_new.html')
	
class FunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		#chrome_options.add_argument('--dns-prefetch-disable')
		#chrome_options.add_argument('--no-sandbox')        
		#chrome_options.add_argument('--headless')
		#chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTest, self).tearDown()

	def test_input_todo(self):
		selenium = self.selenium
        # Opening the link we want to test and delay it to see if it works
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		self.assertEqual('HOMEPAGE', selenium.title) #Cek kalau title webdriver sesuai dengan yang title page
		status_page = selenium.find_element_by_id('AddStatusButton')
		
		#Tes jika ada properti css
		
		button_classes = selenium.find_elements_by_class_name("btn")
		button_class1 = button_classes[0]
		button_class2 = button_classes[1]
		self.assertEqual("righteous", button_class1.value_of_css_property("font-family"))
		self.assertEqual("25px", button_class2.value_of_css_property("font-size"))
		
		self.assertEqual('Add status!', status_page.text) #Cek kalau text pada button sesuai dengan yang diminta
		status_page.click()
		time.sleep(3)
		title = selenium.find_element_by_id('id_title')
		description = selenium.find_element_by_id('id_description')
		submit = selenium.find_element_by_id('submitButton')

        # Fill the form with data
		title.send_keys('Story 7')
		time.sleep(2)
		description.send_keys('Coba-Coba')
		time.sleep(2)

        # submitting the form
		submit.send_keys(Keys.RETURN)
		time.sleep(3)
		selenium.execute_script("window.scrollTo(0, document.body.scrollHeight);")
		elements = selenium.find_elements_by_id('statusTitle')
		size = len(elements)
		self.assertTrue(size > 0) #Cek kalau status ada atau tidak
		target_element = elements[size-1]
		self.assertIn('Story 7', target_element.text)
		time.sleep(2)
		target_element.click()
		time.sleep(5)
	
	def test_JQuery_theme(self):
		selenium = self.selenium
        # Opening the link we want to test and delay it to see if it works
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		self.assertEqual('HOMEPAGE', selenium.title) #Cek kalau title webdriver sesuai dengan yang title page
		theme_page = selenium.find_element_by_id('ThemeButton');
		theme_page.click()
		time.sleep(2)
		themeButton1 = selenium.find_element_by_xpath("//button[@value='a']")
		themeButton2 = selenium.find_element_by_xpath("//button[@value='b']")
		themeButton2.click()
		time.sleep(2)
		themeButton1.click()
		time.sleep(5)
		
	def test_JQuery_accordion(self):
		selenium = self.selenium
        # Opening the link we want to test and delay it to see if it works
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		self.assertEqual('HOMEPAGE', selenium.title) #Cek kalau title webdriver sesuai dengan yang title page
		profile_page = selenium.find_element_by_id('ProfileButton');
		profile_page.click()
		time.sleep(2)
		selenium.execute_script("window.scrollTo(0, document.body.scrollHeight);")
		accordions = selenium.find_elements_by_xpath("//div[contains(@class,'ziehharmonika')]/h3")
		accordions[0].click()
		time.sleep(1)
		accordions[1].click()
		time.sleep(1)
		accordions[2].click()
		time.sleep(1)
		accordions[2].click()
		time.sleep(5)
		
	def test_book(self):
		selenium = self.selenium
        # Opening the link we want to test and delay it to see if it works
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		self.assertEqual('HOMEPAGE', selenium.title) #Cek kalau title webdriver sesuai dengan yang title page
		profile_page = selenium.find_element_by_id('BookButton');
		profile_page.click()
		time.sleep(2)
		title = selenium.find_element_by_id('judul')
		submit = selenium.find_element_by_id('submitButton2')

        # Fill the search bar with data
		title.send_keys('quail')
		time.sleep(2)

        # submitting
		submit.send_keys(Keys.RETURN)
		time.sleep(3)
		star = selenium.find_element_by_id('star 1')
		#counter = selenium.find_element_by_id('counter')
		star.click()
		time.sleep(3)
		#self.assertEqual('1', counter.text)
		time.sleep(5)
		star.click()
		time.sleep(3)
		#self.assertEqual('0', counter.text)
		time.sleep(5)
		
	def test_registration(self):
		selenium = self.selenium
        # Opening the link we want to test and delay it to see if it works
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(5)
		self.assertEqual('HOMEPAGE', selenium.title) #Cek kalau title webdriver sesuai dengan yang title page
		regis_page = selenium.find_element_by_id('RegisterButton');
		regis_page.click()
		time.sleep(2)
		username = selenium.find_element_by_id('username')
		password = selenium.find_element_by_id('password')
		email = selenium.find_element_by_id('email')
		submit = selenium.find_element_by_id('submitButtonRegis')

        # Fill the search bar with data
		username.send_keys('quail')
		time.sleep(2)
		password.send_keys('testpass18')
		time.sleep(2)
		email.send_keys('ghdahs@hse.com')
		time.sleep(2)

        # submitting
		submit.send_keys(Keys.RETURN)
		time.sleep(3)

