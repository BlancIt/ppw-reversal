from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.urls import reverse

class Status(models.Model):
	title = models.CharField(max_length=300)
	description = models.TextField()
	published_date = models.DateTimeField(auto_now_add=True, editable=False)

	def submit(self):
		self.published_date = timezone.now()
		self.save()

	def __str__(self):
		return self.title
		
	def get_absolute_url(self):
		return reverse('status_detail', kwargs={'pk': self.pk})	
		
	class Meta:
		verbose_name_plural = "status"

class Register(models.Model):
	username = models.CharField(max_length = 200)
	password = models.CharField(max_length = 200)
	email = models.EmailField(max_length = 200)
