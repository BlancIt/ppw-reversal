from django.test import TestCase
from .models import Status
from django.contrib.auth import get_user_model
from django.test import Client
from django.urls import resolve
from .views import index, status_new, profile
from .forms import PostForm
from .models import Status
from django.contrib.auth.models import User
#from .forms import Todo_Form

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

browser = webdriver.Firefox()
browser.get('http://localhost:8000')
assert 'To-Do' in browser.title
browser.quit()