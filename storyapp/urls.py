from django.urls import path
from django.conf.urls import include, url
from django.contrib import admin
from .forms import PostForm
from .forms import RegistrationForm

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('auth/', include('social_django.urls', namespace='social')),
	path('new/', views.status_new, name='status_new'),
	path('login/', views.login, name='login'),
	path('profile/', views.profile, name='profile'),
    path('status/<int:pk>/', views.status_detail, name='status_detail'),
	path('theme/', views.change_theme, name='change_theme'),
	path('book/', views.book, name='book'),
	path('book_data/', views.book_data, name='book_data'),
	path('registration/', views.registration, name='registration'),
	path('email/<str:email>', views.emailChecker, name='EmailChecker'),
	path('account/', views.accountList, name='account'),
	path('account/<str:email>', views.deleteAccount, name='deleteAccount'),
	path('deleteAll', views.deleteAccountAll, name='deleteAccountAll'),
]