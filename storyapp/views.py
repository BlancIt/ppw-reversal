from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from django.contrib.auth import authenticate, login
#from .forms import PostForm
from django.utils import timezone
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView
from django.views.generic.base import TemplateView
from .forms import *
import requests
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

site = ['HOMEPAGE', 'ADD STATUS', 'STATUS', 'THEME', 'BOOK', 'REGISTER']

def index(request):
	status_list = Status.objects.filter(published_date__lte=timezone.now()).order_by('published_date')

	for key, value in request.session.items():
		print('{} => {}'.format(key, value))
		print(key)
		print(value)
	
	return render(request, 'index.html', {'status_list': status_list, 'sitename': site[0]})
	
def profile(request):
	response = {'sitename': site[0]}
	return render(request, 'profile.html', response)
	
def login(request):
	response = {'sitename': 'LOGIN'}
	return render(request, 'login.html', response)
	
def status_new(request):
	if request.method == "POST":
		form = PostForm(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.published_date = timezone.now()
			post.save()
			return redirect('/')
	else:
		form = PostForm()
	return render(request, 'status_new.html', {'form': form, 'sitename': site[1]})
	
def status_detail(request, pk):
	status = get_object_or_404(Status, pk=pk)
	return render(request, 'status_detail.html', {'status': status, 'sitename': site[2]})
	
def change_theme(request):
	return render(request, 'change_theme.html', {'sitename': site[3]})
	
def book(request):
	return render(request, 'book.html', {'sitename': site[4]})
	
def registration(request):
	if request.method == "POST":
		form = RegistrationForm(request.POST)
		if form.is_valid():
			post = form.save()
			print("Masuk")
	else:
		form = RegistrationForm()
	return render(request, 'register.html', {'form': form, 'sitename': site[5]})
	
def book_data(request):
	title = request.GET.get('titleSearch')
	print(title)
	data = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + title).json()
	return JsonResponse(data)
	
def emailChecker(request, email):
	emptyEmail = Register.objects.filter(email=email).count() == 0
	json_data = {'isEmpty': emptyEmail}
	return JsonResponse(json_data)
	
def accountList(request):
	dict_data = []
	data = serializers.serialize("json", Register.objects.all())
	return JsonResponse(data, safe = False)

@csrf_exempt	
def deleteAccount(request, email):
	account = get_object_or_404(Register, email=email)
	account.delete()
	return HttpResponse('OK')

def deleteAccountAll(request):
	account = Register.objects.all()
	account.delete()
	return HttpResponse('All deleted!')
