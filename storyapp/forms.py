from django import forms
from django.shortcuts import render, get_object_or_404
from django.forms.widgets import *

from .models import Status
from .models import Register

class PostForm(forms.ModelForm):

	class Meta:
		model = Status
		fields = ('title', 'description')
		labels = {
		'title':'Nama',
		'description':'Status'
		}
		widgets = {
		'title': forms.TextInput(attrs={'class':'form-control'}),
		'description': forms.Textarea(attrs={'class':'form-control', 'placeholder':'Isi statusmu disini'}),
		}
		
class RegistrationForm(forms.ModelForm):
	class Meta:
		model = Register
		fields = ['username', 'password', 'email']
		fields_required = ['username', 'password', 'email']
		widgets = {
			'username': forms.TextInput(attrs={'class': 'form-control my-2', 'placeholder': 'Enter your username', 'id': 'username'}),
			'password': forms.PasswordInput(attrs={'class': 'form-control my-2', 'placeholder': 'Enter your password', 'id': 'password'}),
			'email': forms.EmailInput(attrs={'class': 'form-control my-2', 'placeholder': 'Enter your email', 'id': 'email'}),
		}