signed_in = false;

$(document).ready(function() {
	$(document).on(function() {
		restriction();
	});
	restriction();
})

function restriction() {
	if (signed_in == true) {
		$('#AddStatusButton').removeAttr("disabled");
		$('#ProfileButton').removeAttr("disabled");
		$('#BookButton').removeAttr("disabled");
		
	} else {
		$('#AddStatusButton').attr("disabled", "disabled");
		$('#BookButton').attr("disabled", "disabled");
		$('ProfileButton').attr("disabled", "disabled");
	}
}

function onSignIn(googleUser) {
	signed_in = true;
	var profile = googleUser.getBasicProfile();
	profName = profile.getName();
	$("#welcomeText").text("Welcome! " + profName);
	$('#SO').removeAttr("disabled");
	restriction();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profName);
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
	signed_in = false;
	restriction();
	$('#SO').attr("disabled", "disabled");
	var auth2 = gapi.auth2.getAuthInstance();
	auth2.signOut().then(function () {
		console.log('User signed out.');
		alert("You have successfully logged out!");
	});
}

/**
 * The Sign-In client object.
 */
var auth2;

/**
 * Initializes the Sign-In client.
 */
var initClient = function() {
    gapi.load('auth2', function(){
        /**
         * Retrieve the singleton for the GoogleAuth library and set up the
         * client.
         */
        auth2 = gapi.auth2.init({
            client_id: '526737419304-j6vbinf4q918jus161f7oj0spm9ap0j3.apps.googleusercontent.com'
        });

        // Attach the click handler to the sign-in button
        auth2.attachClickHandler('signin-button', {}, onSuccess, onFailure);
    });
};

/**
 * Handle successful sign-ins.
 */
var onSuccess = function(user) {
    console.log('Signed in as ' + user.getBasicProfile().getName());
 };

/**
 * Handle sign-in failures.
 */
var onFailure = function(error) {
    console.log(error);
};