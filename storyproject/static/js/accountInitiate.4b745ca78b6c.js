$(document).ready(function() {
	$('#submitButtonShowAccount').click(function() {
		console.log("Starting...");
		$.ajax({
			url: "/account/",
			dataType: "json",
			success: result => {
				var obj = JSON.parse(result);
				console.log("parse success!");
				console.log(result);
				const data = obj;
				$('#tableBody2').empty();
				for (var i = 0; i < data.length; i++) {
					var number = i + 1;
					var name = data[i]["fields"]["username"];
					var email = data[i]["fields"]["email"];
					var buttonUn= $('<input type="button" value="Unregister" id="' + email + '" onClick="deleted(id)"/>');
					var star = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
					$('<tr id="'+ email +'">').append( 
						$('<td>').text(number),
						$('<td>').text(name),
						$('<td>').html(buttonUn),
					).appendTo('#tableBody2');
				console.log("Success!");
				};
			}
		});
	});
});

function deleted(id) {
	console.log("Email: "+ id);
	var accountEmail = document.getElementById(id);
	console.log("Deleted");
	$.ajax({
		url: "/account/" + id,
		method: 'DELETE',
		success: result => {
			prompt("Please enter the password");
			if (accountEmail != null) {
				accountEmail.remove();
				console.log("Deleted");
			}
		}
	})
}

