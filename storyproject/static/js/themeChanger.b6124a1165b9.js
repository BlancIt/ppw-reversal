$(document).ready(function() {
    $('[name="themeChange"]').click(function() {
		if (this.value == 'a') {
			$('#base').css('background-color', '#3AAFA9');
			$('[name="header"]').removeAttr('background-color');
		}
		else if (this.value == 'b') {
			$('#base').css('background-color', '#FFBA5C');
			$('[name="header"]').css('background-color', '#00A6FF');
		}
    });
});