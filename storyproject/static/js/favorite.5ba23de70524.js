var off = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
var on = 'https://img.icons8.com/ios/50/f39c12/christmas-star-filled.png';
var count = 0;

$('#star').on("click", function(e){
	console.log("Clicked");
	var current_element = $(e.target);
	var id = current_element.id;
	var source = current_element.src;
	if (source == off) {
		source = on;
		count++;
		console.log("Count up!");
	} else {
		source = off;
		count--;
		console.log("Count down!");
	}
});
