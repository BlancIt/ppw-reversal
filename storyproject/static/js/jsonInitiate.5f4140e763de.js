$(document).ready(function() {
	console.log("Starting...");
	$.ajax({
		url: "/book_data/",
		dataType: "json",
		success: result => {
			console.log("Trying get data...");
			const data = result.items;
			for (var i = 0; i < data.length; i++) {
				var number = i + 1;
				var title = data[i]["volumeInfo"]["title"];
				var publisher = data[i]["volumeInfo"]["publisher"];
				var authors = data[i]["volumeInfo"]["authors"].toString();
				var description = data[i]["volumeInfo"]["description"];
				var coverArt = data[i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
				var star = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
				$('<tr>').append( 
					$('<td>').text(number),
					$('<td>').text(title),
					$('<td>').text(authors),
					$('<td>').text(publisher),
					$('<td>').text(description),
					$('<td>').html("<img src='" + coverArt +"'>"),
					$('<td>').html("<img src='" + star +"' class='test' id='star " + number + "'>"),
				).appendTo('#tableBody');
			console.log("Success!");
			};
		}
	});
});