$(document).ready(function() {
	console.log("Starting...");
	$.ajax({
		url: "/book_data/",
		dataType: "json",
		success: result => {
			console.log("Trying get data...");
			const data = result.items;
			for (var i = 0; i < data.length; i++) {
				var title = data[i]["volumeInfo"]["title"];
				var publishInfo = data[i]["volumeInfo"]["publisher"];
				var authors = data[i]["volumeInfo"]["authors"].toString();
				$('<tr>').append(
					$('<td>').text(i),
					$('<td>').text(title),
					$('<td>').text(authors),
					$('<td>').text(publishInfo)
				).appendTo('#tableBody');
			console.log("Success!");
			};
		}
	});
});