var available = false;
$(document).ready(function() {
	//Initialize
	$('#submitButtonRegis').attr("disabled", "disabled");
	$(document).on('keyup', '#username, #email, #password', function() {
		checkInput();
	});

	$('#email').on('keyup', function() {
		url = '/email/' + $('#email').val();
		console.log(url);
		$.ajax({
			url: url,
			method: 'GET',
			data: $('#email').val(),
			success: function(response) {
				console.log(response);
				if (response.isEmpty) {
					console.log("Email is not used. You can use it!");
					available = true;
				} else {
					console.log("Email is already used. Please change to another one");
					available = false;
				}
			checkInput();
			}
		});
	});

	$("#registerForm").on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: "/registration/",
			data: {
				username: $("#username").val(),
				email: $("#email").val(),
				password: $("#password").val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
			},
			beforeSend: function(request) {
				return request.setRequestHeader('X-CSRF-Token', $("meta[name='token']").attr('content'));
			},
			success: function(response) {
				alert($("#username").val() + " has been successfully registered!");
				$("#username").val("");
				$("#email").val("");
				$("#password").val("");
				console.log("REGISTERED");
				pageRedirect();
			}
		})
	});
});

function checkInput() {
	if ($('#username').val() && $("#password").val() && $("#email").val() && available) {
		$('#submitButtonRegis').removeAttr("disabled");
	} else {
		$('#submitButtonRegis').attr("disabled", "disabled");
	}
	console.log('Respond is ' + available);
}

function pageRedirect() {
    window.location.replace("/");
}      
