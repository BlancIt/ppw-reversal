$("#jsonAdder").click(() => {
	console.log("test");
	$.ajax({
		url: "/static/json/bookView.json",
		success: result => {
			const data = result.items;
			$.each(data, function(i, item) {
				$('<tr>').append(
					$('<td>').text(item.volumeInfo.title),
					$('<td>').text(item.volumeInfo.subtitle),
					$('<td>').text(item.publisher)
				).appendTo('#records_table');
			});
		}
	});
});