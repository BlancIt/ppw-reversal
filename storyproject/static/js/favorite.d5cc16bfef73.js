var off = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
var on = 'https://img.icons8.com/ios/50/f39c12/christmas-star-filled.png';
var count = 0;

function favorite(id) {
	console.log("Star " + id + " clicked");
	var current = document.getElementById(id);
	if(current.src == staroff) {
		console.log("Star on");
		count++;
		current.src = staron;
		console.log(count);
	} else {
		console.log("Star off");
		count--;
		current.src = staroff;
		console.log(count);
	}
	document.getElementById("counter").innerHTML = count;
}
