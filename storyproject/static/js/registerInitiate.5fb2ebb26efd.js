var available = false;
$(document).ready(function() {
	//Initialize
	checkInput();
	$(document).on('keyup', '#username, #email, #password', function() {
		checkInput();
	});

	$('#email').on('change', function() {
		url = '/email/' + $('#email').val();
		console.log(url);
		$.ajax({
			url: url,
			method: 'GET',
			data: $('#email').val(),
			success: function(response) {
				console.log(response);
				if (response.isOk) {
					console.log("Email is OK");
				} else {
					console.log("Email is not OK");
				}
			}
		}).then(function(response) {
			if (response.isOk) {
				available = true;
			}
			console.log("available: " + available);
			checkInput();
		});
	});

	$("#registerForm").on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: "/registration",
			data: {
				username: $("#username").val(),
				email: $("#email").val(),
				password: $("#password").val(),
				csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val(),
			},
			success: function(response) {
				alert($("#username").val() + " has been registered as an user!");
				$("#username").val("");
				$("#email").val("");
				$("#password").val("");
				console.log("OK");
			}
		})
	});
});

function checkInput() {
	console.log('Checking Form Input'); 
	if ($('#username').val() && $("#password").val() && $("#email").val() && available) {
		$('#submitButton').removeAttr("disabled");
	} else {
		$('#submitButton').attr("disabled", "disabled");
	}
	console.log(available);
}
