$(document).ready(function() {
	console.log("Starting...");
	$.ajax({
		url: "/book_data/",
		dataType: "json",
		success: result => {
			console.log("Trying get data...");
			const data = result.items;
			for (var i = 0; i < data.length; i++) {
				var number = i + 1;
				var title = data[i]["volumeInfo"]["title"];
				var publisher = data[i]["volumeInfo"]["publisher"];
				var authors = data[i]["volumeInfo"]["authors"].toString();
				var description = data[i]["volumeInfo"]["description"];
				var coverArt = data[i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
				$('<tr>').append(
					$('<td>').text(number),
					$('<td>').text(title),
					$('<td>').text(authors),
					$('<td>').text(publisher)
					$('<td>').text(description)
					$('<td>').image(coverArt)
				).appendTo('#tableBody');
			console.log("Success!");
			};
		}
	});
});