$(document).ready(function() {
	$('#submitButtonShowAccount').click(function() {
		console.log("Starting...");
		$.ajax({
			url: "/account/",
			dataType: "json",
			success: result => {
				var obj = JSON.parse(result);
				console.log("parse success!");
				console.log(result);
				const data = obj;
				$('#tableBody').empty();
				for (var i = 0; i < data.length; i++) {
					var number = i + 1;
					var name = data[i]["fields"]["username"];
					var star = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
					$('<tr>').append( 
						$('<td>').text(number),
						$('<td>').text(name),
						$('<td>').html("<button img src= '" + star +"' class='unregisterButton' id='unregister " + number + ">"),
					).appendTo('#tableBody2');
				console.log("Success!");
				};
			}
		});
	});
});