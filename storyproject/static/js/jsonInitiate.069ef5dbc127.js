var off = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
var on = 'https://img.icons8.com/ios/50/f39c12/christmas-star-filled.png';
var count = 0;

$(document).ready(function() {
	$('#submitButton2').click(function() {
		console.log("Starting...");
		$.ajax({
			url: "/book_data/",
			data: { titleSearch: '$("#judul").val'},
			dataType: "json",
			success: result => {
				console.log("Trying get data...");
				const data = result.items;
				for (var i = 0; i < data.length; i++) {
					var number = i + 1;
					var title = data[i]["volumeInfo"]["title"];
					var publisher = data[i]["volumeInfo"]["publisher"];
					var authors = data[i]["volumeInfo"]["authors"].toString();
					var description = data[i]["volumeInfo"]["description"];
					var coverArt = data[i]["volumeInfo"]["imageLinks"]["smallThumbnail"];
					var star = 'https://img.icons8.com/ios/50/000000/christmas-star.png';
					$('<tr>').append( 
						$('<td>').text(number),
						$('<td>').text(title),
						$('<td>').text(authors),
						$('<td>').text(publisher),
						$('<td>').text(description),
						$('<td>').html("<img src='" + coverArt +"'>"),
						$('<td>').html("<img src='" + star +"' class='test' id='star " + number + "' onClick='favorite(id)'>"),
					).appendTo('#tableBody');
				console.log("Success!");
				};
			}
		});
	});
});

function favorite(id) {
	console.log("Star " + id + " clicked");
	var current = document.getElementById(id);
	if(current.src == off) {
		console.log("Star increased!");
		count++;
		current.src = on;
		console.log(count);
	} else {
		console.log("Star decreased!");
		count--;
		current.src = off;
		console.log(count);
	}
	document.getElementById("counter").innerHTML = count;
}